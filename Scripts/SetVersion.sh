#!/bin/bash

GIT_VERSION="$(Scripts/GetVersion.sh)"
PATH_TO_VERSION_FILE=Global/Version.gd

echo "Setting version to $GIT_VERSION"

sed -i "/var game_version = UNKNOWN_VERSION/c\var game_version = \"$GIT_VERSION\"" "$PATH_TO_VERSION_FILE"
