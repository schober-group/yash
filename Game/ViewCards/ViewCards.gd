extends PopupPanel

onready var cards_to_view = $CenterContainer/MarginContainer/VBoxContainer/CardsContainer.get_children()

export var img_policy_fascist: ImageTexture
export var img_policy_liberal: ImageTexture
export var img_role_fascist: ImageTexture
export var img_role_liberal: ImageTexture


func _ready():
	for card_rect in cards_to_view:
		card_rect.hide()


func show_cards(cards: Array):
	print("Showing cards")
	var card_type
	for card_rect in cards_to_view:
		card_type = cards[card_rect.get_index()]
		card_rect.show()
		match card_type:
			Cards.card_type.FASCIST:
				card_rect.texture = img_policy_fascist
				print("CARD %d is Fascist" % card_rect.get_index())
			Cards.card_type.LIBERAL:
				card_rect.texture = img_policy_liberal
				print("CARD %d is Liberal" % card_rect.get_index())
			_:
				pass
	popup()


func show_roles(role: int):
	print("Showing role: %d" % role)
	cards_to_view[0].show()
	match role:
		PlayerConst.player_roles.FASCIST:
			cards_to_view[0].texture = img_role_fascist
			print("ROLE IS FASCIST")
		PlayerConst.player_roles.LIBERAL:
			cards_to_view[0].texture = img_role_liberal
			print("ROLE IS LIBERAL")
		_:
			pass
	popup()


func _on_Button_pressed():
	GameHelper.game.rpc_id(1, "next_president_server")
	hide()
