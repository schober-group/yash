extends Control

class_name SelectPlayerButton

signal hide_other_confirm_action
signal player_selected(player, type)

var player_on_button: Player setget set_player
var type_on_selection: int

onready var confirm_action_container = $PlayerName/ConfirmActionContainer
onready var lbl_player = $PlayerName


func set_player(player: Player):
	player_on_button = player
	lbl_player.text = player_on_button.player_name


func hide_confirm_action():
	confirm_action_container.hide()


func refresh_status(type: int):
	var tooltip = "Can be selected"
	type_on_selection = type
	lbl_player.disabled = false
	match type:
		GameConst.voting_types.CHANCELLOR:
			if ! PlayerHelper.is_valid_chancellor(player_on_button):
				lbl_player.disabled = true
			if player_on_button.is_dead:
				tooltip = "This Player is dead"
			elif player_on_button.president_info == PlayerConst.president_state.CURR_PRESIDENT:
				tooltip = "You are the President"
			elif player_on_button.chancellor_info == PlayerConst.chancellor_state.LAST_CHANCELLOR:
				tooltip = "This Player was the last Chancellor"
			elif player_on_button.president_info == PlayerConst.president_state.LAST_PRESIDENT:
				tooltip = "This Player was the last President"
		GameConst.voting_types.KILL_PLAYER:
			if ! PlayerHelper.is_killable(player_on_button):
				lbl_player.disabled = true
			if player_on_button.is_dead:
				tooltip = "This Player is dead"
			elif player_on_button.president_info == PlayerConst.president_state.CURR_PRESIDENT:
				tooltip = "You are the President"
		GameConst.voting_types.PRESIDENT:
			if ! PlayerHelper.is_valid_president(player_on_button):
				lbl_player.disabled = true
			if player_on_button.is_dead:
				tooltip = "This Player is dead"
			elif player_on_button.president_info == PlayerConst.president_state.CURR_PRESIDENT:
				tooltip = "You are the President"
		GameConst.voting_types.INVESTIGATE:
			if ! PlayerHelper.is_investigatable(player_on_button):
				lbl_player.disabled = true
			if player_on_button.is_dead:
				tooltip = "This Player is dead"
			elif player_on_button.president_info == PlayerConst.president_state.CURR_PRESIDENT:
				tooltip = "You are the President"

	lbl_player.hint_tooltip = tooltip


func _on_PlayerName_pressed():
	emit_signal("hide_other_confirm_action")
	confirm_action_container.show()


func _on_PlayerName_focus_entered():
	if not lbl_player.disabled:
		_on_PlayerName_pressed()


func _on_Yes_pressed():
	emit_signal("player_selected", player_on_button, type_on_selection)


func _on_No_pressed():
	hide_confirm_action()
