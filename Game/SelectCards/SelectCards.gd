extends PopupPanel

enum STATE { president, chancellor }

onready var cards_container = $MarginContainer/CardsContainer
onready var card3 = cards_container.get_node("Card3")


func _ready():
	for card in cards_container.get_children():
		card.connect("card_selected", self, "_card_selected")


func display(_cards: Array):
	match _cards.size():
		2:
			_show_2_cards()
		3:
			_show_3_cards()
		_:
			print("Invalid!")

	_set_cards(_cards)
	popup()


func _set_cards(_cards: Array):
	assert(2 == _cards.size() or 3 == _cards.size())
	for i in _cards.size():
		_set_card(cards_container.get_child(i), _cards[i])


func _set_card(card: Node, type: int):
	card.card_type = type


func _show_3_cards():
	card3.show()


func _show_2_cards():
	card3.hide()


func _card_selected(card_type: int):  # Card which should be discarded
	print("Card selected: %s" % card_type)
	rpc_id(1, "_card_selected_server", card_type)


master func _card_selected_server(card_type: int):  # Card which should be discarded
	assert(get_tree().is_network_server())
	print("Discarding: %s" % GameHelper.get_card_type_as_string(card_type))
	print("Client ID: %s" % get_tree().get_rpc_sender_id())
	match get_tree().get_rpc_sender_id():
		GameHelper.game.president_id:
			_remove_card(card_type, 3)
			GameHelper.game.emit_signal("president_voted")
		GameHelper.game.chancellor_id:
			_remove_card(card_type, 2)
			GameHelper.game.emit_signal("chancellor_voted")

	rpc_id(get_tree().get_rpc_sender_id(), "close")


func _remove_card(card_type: int, drawn_cards: int):
	var card_index = GameHelper.get_index_of_selected_card(card_type, drawn_cards)
	assert(card_index != -1)
	GameHelper.game.discard_card(card_index)


remotesync func close():
	hide()
