extends PanelContainer

export var fascist: bool = true

onready var vbox = $MarginContainer/VBoxContainer

const BOARD_ELEMENT = "res://Game/Board/BoardElement.tscn"

func set_board(player_count: int):
	_clear()
	
	var new_style
	
	if fascist:
		new_style = load("res://GUI/Theme/Board/Fascist/Board.tres")
		match player_count:
			5, 6:
				_add_element("Fascists win")
				_add_element("Kill a player", "BulletIcon")
				_add_element("Kill a player", "BulletIcon")
				_add_element("Show next 3 cards", "DrawCards")
				_add_element()
				_add_element()
			7,8:
				_add_element("Fascists win")
				_add_element("Kill a player", "BulletIcon")
				_add_element("Kill a player", "BulletIcon")
				_add_element("Select next president", "NextPresident")
				_add_element("Show player role", "Magnifier")
				_add_element()	
			9,10:
				_add_element("Fascists win")
				_add_element("Kill a player", "BulletIcon")
				_add_element("Kill a player", "BulletIcon")
				_add_element("Select next president", "NextPresident")
				_add_element("Show player role", "Magnifier")
				_add_element("Show player role", "Magnifier")
			_:
				print("Invalid Player count")
	else:
		new_style = load("res://GUI/Theme/Board/Liberal/Board.tres")
		for _i in range(5):
			_add_element()

	_override_style(new_style)

func set_cards(amount: int):
	var child_count = vbox.get_child_count()
	assert(amount <= child_count)
	
	for i in range(amount):
		vbox.get_child(child_count - i - 1).enable()


func _override_style(new_style: StyleBox):
	add_stylebox_override("panel", new_style)


func _add_element(short_info: String = "Nothing", texture: String = "Empty"):
	var element = preload(BOARD_ELEMENT).instance()
	element.short_info = short_info
	element.texture = texture
	element.fascist = fascist
	
	vbox.add_child(element)

func _clear():
	for element in vbox.get_children():
		vbox.remove_child(element)
		element.queue_free()
