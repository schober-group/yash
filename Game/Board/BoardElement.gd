extends Button

export var short_info: String
export var texture: String
export var fascist: bool

onready var short_info_label: RichTextLabel = $MarginContainer/HBoxContainer/ShortInfoLabel
onready var texture_rect: TextureRect = $MarginContainer/HBoxContainer/Texture
onready var animation: AnimationPlayer = $AnimationPlayer
onready var board: PanelContainer = $"../../.."

var open: bool = false


func _ready():
	short_info_label.bbcode_text = "[center]%s[/center]" % short_info
	texture_rect.texture = load("res://Assets/Board/" + texture + ".svg")

	var new_style

	if fascist:
		new_style = load("res://GUI/Theme/Board/Fascist/Normal.tres")
	else:
		new_style = load("res://GUI/Theme/Board/Liberal/Normal.tres")

	_override_style(new_style)


func enable():
	var new_style

	if fascist:
		new_style = load("res://GUI/Theme/Board/Fascist/Enabled.tres")
	else:
		new_style = load("res://GUI/Theme/Board/Liberal/Enabled.tres")

	_override_style(new_style)


func _override_style(new_style: StyleBox):
	add_stylebox_override("normal", new_style)
	add_stylebox_override("pressed", new_style)
	add_stylebox_override("focus", new_style)
	add_stylebox_override("hover", new_style)


func _on_PanelContainer_pressed():
	var animation_name

	if fascist:
		animation_name = "open_left_to_right"
	else:
		animation_name = "open_right_to_left"

	if ! open:
		animation.play(animation_name)
	else:
		animation.play_backwards(animation_name)

	open = ! open
