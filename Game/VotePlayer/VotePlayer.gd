extends PopupPanel

onready var voting_dialog = $CenterContainer/VBoxContainer/VotingDialog

enum vote_type { YES, NO }

var yes_votes = 0
var no_votes = 0
var player_nominated: Player


func init():
	var _error = GameHelper.game.connect("chancellor_nominated", self, "_chancellor_nominated")
	assert(_error == OK)


func _chancellor_nominated(player: Player):
	print("Chancellor nominated: %s" % player.player_name)
	player_nominated = player
	_set_dialog()
	if ! PlayerHelper.get_player_by_id(get_tree().get_network_unique_id()).is_dead:
		popup()


func _set_dialog():
	var president = PlayerHelper.get_president()
	voting_dialog.text = (
		"Do you want '%s\'' to be chancellor?\n('%s' is president)"
		% [player_nominated.player_name, president.player_name]
	)


func _on_Yes_pressed():
	rpc_id(1, "_count_votes", vote_type.YES)
	hide()


func _on_No_pressed():
	rpc_id(1, "_count_votes", vote_type.NO)
	hide()


master func _count_votes(vote):
	match vote:
		vote_type.YES:
			yes_votes += 1
		vote_type.NO:
			no_votes += 1
		_:
			print("Invalid vote!")

	if yes_votes + no_votes == PlayerHelper.get_alive_player_count():
		_eval_votes()


func _eval_votes():
	print("Election results: %d / %d" % [yes_votes, no_votes])
	if yes_votes > no_votes:
		PlayerHelper.set_chancellor(player_nominated)
		GameHelper.game.emit_signal("government_passed")
	else:
		GameHelper.game.rpc("government_failed")
	_reset_votes()


func _reset_votes():
	yes_votes = 0
	no_votes = 0
