extends Button

signal card_selected(card_type)

var card_type: int setget set_card_type


func set_card_type(_card_type):
	card_type = _card_type
	# TODO Refresh img

	match card_type:
		Cards.card_type.LIBERAL:
			text = "Liberal"
		Cards.card_type.FASCIST:
			text = "Fascist"
		_:
			text = "?"


func _on_Card_pressed():
	print("Player selected card")
	emit_signal("card_selected", card_type)
