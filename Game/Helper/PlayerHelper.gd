extends Node


func get_player_by_id(player_id: int) -> Player:
	return GameHelper.players.get_node(str(player_id)) as Player


func get_alive_player_count():
	var player_count = 0
	for player in GameHelper.get_players():
		if ! player.is_dead:
			player_count += 1
	return player_count as int


func get_president() -> Player:
	return get_player_by_id(GameHelper.game.president_id) as Player


func get_last_president() -> Player:
	return get_player_by_id(GameHelper.game.last_president_id) as Player


func get_random_player() -> Player:
	var total = GameHelper.players.get_child_count()
	var num = randi() % total
	return GameHelper.players.get_child(num) as Player


func set_player_roles():
	var total = _get_number_of_players()
	var fascists = _get_number_of_fascists(total)
	var liberal = _get_number_of_liberals(total)
	var player_roles: Array = []

	for i in range(total):
		if i < fascists:
			player_roles.append(PlayerConst.player_roles.FASCIST)
		elif i < liberal + fascists:
			player_roles.append(PlayerConst.player_roles.LIBERAL)
		else:
			player_roles.append(PlayerConst.player_roles.HITLER)

	player_roles.shuffle()

	for i in range(total):
		var player = GameHelper.players.get_child(i)
		player.role = player_roles[i]
		player.rpc_id(player.player_id, "set_role", player.role)


func is_valid_chancellor(player: Player) -> bool:
	if GameHelper.game.players_left > 5:
		return ! (
			player.is_dead == true
			or player.chancellor_info == PlayerConst.chancellor_state.LAST_CHANCELLOR
			or player.president_info == PlayerConst.president_state.CURR_PRESIDENT
			or player.president_info == PlayerConst.president_state.LAST_PRESIDENT
		)
	else:
		return ! (
			player.is_dead == true
			or player.chancellor_info == PlayerConst.chancellor_state.LAST_CHANCELLOR
			or player.president_info == PlayerConst.president_state.CURR_PRESIDENT
		)


func is_killable(player: Player) -> bool:
	return ! (
		player.is_dead == true
		or player.president_info == PlayerConst.president_state.CURR_PRESIDENT
	)


func is_valid_president(player: Player) -> bool:
	return ! (
		player.is_dead == true
		or player.president_info == PlayerConst.president_state.CURR_PRESIDENT
	)


func is_investigatable(player: Player) -> bool:
	return ! (
		player.is_dead == true
		or player.president_info == PlayerConst.president_state.CURR_PRESIDENT
	)


func get_next_president() -> Player:
	print("Getting next President")
	match GameHelper.game.next_president_after_emergency:
		-1:
			var curr_president_pos = get_president().get_position_in_parent()
			var next_president_pos = curr_president_pos + 1
			var next_president = _get_next_president(next_president_pos)

			while next_president.is_dead:
				next_president_pos += 1
				next_president = _get_next_president(next_president_pos)

			return next_president
		_:
			var next_president
			if ! GameHelper.game.next_president_after_emergency.is_dead:
				next_president = GameHelper.game.next_president_after_emergency
			else:
				print("President was killed, getting next")
				var next_president_pos = (
					GameHelper.game.next_president_after_emergency.get_position_in_parent()
					+ 1
				)
				next_president = _get_next_president(next_president_pos)
				while next_president.is_dead:
					next_president_pos += 1
					next_president = _get_next_president(next_president_pos)
			GameHelper.game.next_president_after_emergency = -1
			return next_president


func _get_next_president(offset: int) -> Player:
	return (
		GameHelper.game.players_node.get_child((offset) % (GameHelper.game.players.size() - 1)) as Player
	)


func set_president(player: Player = get_next_president()):
	rpc(
		"_set_president_approved",
		player.player_id,
		GameHelper.game.president_id,
		GameHelper.game.last_president_id,
		GameHelper.game.chancellor_id,
		GameHelper.game.last_chancellor_id
	)


remotesync func _set_president_approved(
	new_president_id: int,
	old_president_id: int,
	even_older_president_id: int,
	old_chancellor_id: int,
	even_older_chancellor_id: int
):
	assert(1 == get_tree().get_rpc_sender_id())

	var new_president = get_player_by_id(new_president_id)
	new_president.president_info = PlayerConst.president_state.CURR_PRESIDENT
	print("Setting new president: %s" % new_president.player_name)

	if old_president_id != -1:
		var old_president = get_player_by_id(old_president_id)
		old_president.president_info = PlayerConst.president_state.LAST_PRESIDENT

	if even_older_president_id != -1:
		var old_president = get_player_by_id(even_older_president_id)
		old_president.president_info = PlayerConst.president_state.NOT_PRESIDENT

	_age_chancellor(old_chancellor_id, even_older_chancellor_id)


func _age_chancellor(old_chancellor_id: int, even_older_chancellor_id: int):
	if old_chancellor_id != -1:
		var old_chancellor = get_player_by_id(old_chancellor_id)
		old_chancellor.chancellor_info = PlayerConst.chancellor_state.LAST_CHANCELLOR
		GameHelper.game.chancellor_id = -1

	if even_older_chancellor_id != -1:
		var even_older_chancellor = get_player_by_id(even_older_chancellor_id)
		even_older_chancellor.chancellor_info = PlayerConst.chancellor_state.NOT_CHANCELLOR


func set_chancellor(player: Player):
	rpc("_set_chancellor_approved", player.player_id)


remotesync func _set_chancellor_approved(new_chancellor_id: int):
	assert(1 == get_tree().get_rpc_sender_id())
	var new_chancellor = get_player_by_id(new_chancellor_id)
	new_chancellor.chancellor_info = PlayerConst.chancellor_state.CURR_CHANCELLOR
	print("Setting new chancellor: %s" % new_chancellor.player_name)


func _get_number_of_players() -> int:
	return GameHelper.players.get_child_count()


func _get_number_of_fascists(total: int = _get_number_of_players()) -> int:
	match total:
		5:
			return 1
		6:
			return 1
		7:
			return 2
		8:
			return 2
		9:
			return 3
		10:
			return 3
		_:
			return -1


func _get_number_of_liberals(total: int = _get_number_of_players()) -> int:
	return total - _get_number_of_fascists(total) - 1
