extends Node

var game: Game
var players: Node


func init():
	game = get_node(GameConst.game)
	players = game.get_node(GameConst.players)

	game.select_player.init()
	game.government_vote.init()


func get_card_type(index: int) -> int:
	return game.cards[index]


func get_card_type_as_string(card_type: int) -> String:
	return Cards.card_type.keys()[card_type]


func get_index_of_selected_card(card_type: int, drawn_cards: int) -> int:
	assert(game.cards.size() >= drawn_cards)

	for i in range(0, drawn_cards):
		if game.cards[i] == card_type:
			return i

	return -1


func get_players() -> Array:
	return players.get_children()


func get_fascists_name() -> Array:
	var temp: Array
	for p in get_players():
		if p.role == PlayerConst.player_roles.HITLER or p.role == PlayerConst.player_roles.FASCIST:
			temp.append(p.player_name)
	assert(temp != null)
	return temp


func get_liberals_name() -> Array:
	var temp: Array
	for p in get_players():
		if p.role == PlayerConst.player_roles.LIBERAL:
			temp.append(p.player_name)
	assert(temp != null)
	return temp


func get_fascists_id() -> Array:
	var temp: Array
	for p in get_players():
		if p.role == PlayerConst.player_roles.HITLER or p.role == PlayerConst.player_roles.FASCIST:
			temp.append(p.player_id)
	assert(temp != null)
	return temp


func get_liberals_id() -> Array:
	var temp: Array
	for p in get_players():
		if p.role == PlayerConst.player_roles.LIBERAL:
			temp.append(p.player_id)
	assert(temp != null)
	return temp


func get_hitler_id() -> int:
	var temp: int
	for p in get_players():
		if p.role == PlayerConst.player_roles.HITLER:
			temp = p.player_id
	assert(temp != null)
	return temp
