extends PanelContainer

const res_button = "res://Game/SelectPlayerButton/SelectPlayerButton.tscn"

onready var player_buttons = $"MarginContainer/VBoxContainer/ScrollContainer/PlayerButtons"
onready var label = $MarginContainer/VBoxContainer/Label


func init():
	for p in GameHelper.players.get_children():
		_add_player(p)

	_hide_all_buttons()
	_refresh_status(GameConst.voting_types.CHANCELLOR)


func show_chancellor_select():
	_refresh_status(GameConst.voting_types.CHANCELLOR)
	label.text = "Select chancellor"
	.show()


func show_to_kill_select():
	_refresh_status(GameConst.voting_types.KILL_PLAYER)
	label.text = "Select a player to kill"
	.show()


func show_president_select():
	_refresh_status(GameConst.voting_types.PRESIDENT)
	label.text = "Select next president"
	.show()


func show_investigation_select():
	_refresh_status(GameConst.voting_types.INVESTIGATE)
	label.text = "Select a player to investigate"
	.show()


func _refresh_status(type):
	_hide_all_buttons()
	for btn in player_buttons.get_children():
		btn.refresh_status(type)


func _add_player(player: Player):
	var button = preload(res_button).instance()
	player_buttons.add_child(button)
	button.player_on_button = player
	button.connect("hide_other_confirm_action", self, "_hide_all_buttons")
	button.connect("player_selected", self, "_player_selected")


func _hide_all_buttons():
	for btn in player_buttons.get_children():
		btn.hide_confirm_action()


func _player_selected(player: Player, type: int):
	rpc_id(1, "_player_selected_server", player.player_id, type)


master func _player_selected_server(player_id: int, type: int):
	var player = PlayerHelper.get_player_by_id(player_id)
	match type:
		GameConst.voting_types.CHANCELLOR:
			if PlayerHelper.is_valid_chancellor(player):
				rpc("chancellor_selected_approved", player.player_id)
			else:
				print("Invalid chancellor!")
		GameConst.voting_types.KILL_PLAYER:
			if PlayerHelper.is_killable(player):
				rpc("kill_player_approved", player.player_id)
				GameHelper.game.rpc_id(1, "next_president_server")
		GameConst.voting_types.PRESIDENT:
			if PlayerHelper.is_valid_president(player):
				rpc("president_selected_approved", player.player_id)
		GameConst.voting_types.INVESTIGATE:
			if PlayerHelper.is_investigatable(player):
				var role = player.role
				if role == PlayerConst.player_roles.HITLER:
					role = PlayerConst.player_roles.FASCIST
				rpc_id(GameHelper.game.president_id, "investigation_approved", role)

		_:
			print("Unknown state")

remotesync func kill_player_approved(player_id: int):
	assert(get_tree().get_rpc_sender_id() == 1)
	hide()
	var player = PlayerHelper.get_player_by_id(player_id)
	player.kill_player()

remotesync func chancellor_selected_approved(player_id: int):
	assert(get_tree().get_rpc_sender_id() == 1)  # Only accept new chancellor from server
	hide()

	var player = PlayerHelper.get_player_by_id(player_id)
	GameHelper.game.emit_signal("chancellor_nominated", player)

remotesync func president_selected_approved(player_id: int):
	assert(get_tree().get_rpc_sender_id() == 1)  #Only accept new president from server
	hide()

	var player = PlayerHelper.get_player_by_id(player_id)
	GameHelper.game.emit_signal("emergency_president", player)

remotesync func investigation_approved(role: int):
	assert(get_tree().get_rpc_sender_id() == 1)
	hide()

	print("Player selected")
	GameHelper.game.emit_signal("investigate_player", role)
