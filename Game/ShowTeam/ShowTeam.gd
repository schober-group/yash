extends Control

var hidden = true

onready var container = $VBoxContainer/Text/TeamContainer
onready var animation = $AnimationPlayer

func _ready():
	pass


remotesync func show_team_members(team_members, hitler):
	visible = true
	
	for id in parse_json(team_members):
		var player = PlayerHelper.get_player_by_id(id)
		var player_label = Label.new()
		player_label.text = player.player_name
		player_label.name = str(player.player_id)
		if id == hitler:
			player_label.add_color_override("font_color", Color(255, 0, 0))
		container.add_child(player_label)


func _on_Button_pressed():
	if hidden:
		animation.play("open_team")
	else:
		animation.play_backwards("open_team")
	hidden = ! hidden
