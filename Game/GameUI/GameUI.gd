extends Control

class_name Game

# warning-ignore-all:unused_signal
signal chancellor_nominated(player)
signal emergency_president(player)
signal investigate_player(player)
signal government_passed
signal government_failed
signal president_voted
signal chancellor_voted

var is_game_running: bool

var fac_cards = 0
var lib_cards = 0
var gov_failure = 0

var cards: Array = []
var discarded_cards: Array = []

var president_id = -1
var next_president_after_emergency = -1
var chancellor_id = -1
var last_president_id = -1
var last_chancellor_id = -1
var players_left = -1

onready var select_player = $SelectChancellor
onready var government_vote = $GovernmentVote
onready var team_members = $ShowTeam
onready var players_node: Node = $Players
onready var players: Array = players_node.get_children()
onready var select_cards_popup = $SelectCards
onready var view_cards = $ViewCards
onready var temp_role = $tempRole
onready var temp_gov_failure = $tempGovFailure
onready var board_fascist = $BoardFac
onready var board_liberal = $BoardLib


func _ready():
	randomize()
	players_left = Lobby.player_info.size() + 1
	GameHelper.init()

	var _error = OK
	_error += Lobby.connect("start_game", self, "start_game")
	_error += connect("government_passed", self, "government_passed")
	_error += connect("president_voted", self, "president_voted")
	_error += connect("chancellor_voted", self, "chancellor_voted")
	_error += connect("emergency_president", self, "emergency_president")
	_error += connect("investigate_player", self, "investigate_player")
	_error += connect("government_failed", self, "government_failed")
	assert(_error == OK)
	board_fascist.set_board(GameHelper.get_players().size())
	board_liberal.set_board(GameHelper.get_players().size())

	if get_tree().is_network_server(): # FIXME cards not randomized for debugging
		while cards.size() <= 16:
			if cards.size() <= 10:
				cards.append(Cards.card_type.FASCIST)
			elif cards.size() >= 11:
				cards.append(Cards.card_type.LIBERAL)
		#cards[0] = Cards.card_type.FASCIST
		#cards[1] = Cards.card_type.LIBERAL
		#cards[2] = Cards.card_type.FASCIST	
		cards.shuffle()
		
		print("Cards: ", cards)
		PlayerHelper.set_player_roles()
		PlayerHelper.set_president(PlayerHelper.get_random_player())
		refresh_team_members()


func start_game():
	if get_tree().is_network_server():
		rpc_id(president_id, "show_chancellor_choice")


remotesync func show_chancellor_choice():
	assert(1 == get_tree().get_rpc_sender_id())
	print("Showing chancellor choice")
	select_player.show_chancellor_select()
remotesync func show_president_choice():
	assert(1 == get_tree().get_rpc_sender_id())
	print("Showing president choise")
	select_player.show_president_select()

remotesync func show_view_cards(cards_to_show: Array):
	assert(1 == get_tree().get_rpc_sender_id())
	print("Showing top 3 cards on deck")
	view_cards.show_cards(cards_to_show)

remotesync func show_investigation_choice():
	assert(1 == get_tree().get_rpc_sender_id())
	print("Showing president choise")
	select_player.show_investigation_select()

remotesync func show_to_kill_choice():
	assert(1 == get_tree().get_rpc_sender_id())
	print("Showing kill choice")
	select_player.show_to_kill_select()

remotesync func view_role(player_id: int):
	assert(1 == get_tree().get_rpc_sender_id())
	var player = PlayerHelper.get_player_by_id(player_id)
	print("Showing role of player")
	view_cards.show_roles(player.role)


func emergency_president(emer_president: Player):
	print("Setting emergency president: %s" % emer_president.player_name)

	var curr_president: Player = PlayerHelper.get_president()
	curr_president.president_info = PlayerConst.president_state.LAST_PRESIDENT

	next_president_after_emergency = PlayerHelper.get_next_president()
	print("Next President will be %s" % next_president_after_emergency.player_name)
	last_president_id = curr_president.player_id
	emer_president.set_president_info(PlayerConst.president_state.CURR_PRESIDENT)
	president_id = emer_president.player_id
	if get_tree().is_network_server():
		rpc_id(president_id, "show_chancellor_choice")


func investigate_player(role: int):
	print("Investiate player")
	view_cards.show_roles(role)


func government_passed():
	print("Government passed!")

	if (
		GameHelper.game.fac_cards >= 3
		&& GameHelper.game.chancellor_id == GameHelper.get_hitler_id()
	):  # Checks if hitler has been voted chancellor
		Victory.rpc(
			"victory_init",
			to_json(GameHelper.get_liberals_name()),
			to_json(GameHelper.get_fascists_name()),
			GameConst.winning_side.FASCISTS,
			GameConst.winning_reasons.HITLER_IS_CHANCELLOR
		)

	if cards.size() < 3:
		shuffle()

	if get_tree().is_network_server():
		rpc_id(president_id, "select_cards", [cards[0], cards[1], cards[2]])


remotesync func government_failed():
	assert(get_tree().get_rpc_sender_id() == 1)
	print("Government failed!")
	gov_failure += 1
	temp_gov_failure.text = str(gov_failure)

	if gov_failure == 3:
		gov_failure = 0
		temp_gov_failure.text = str(gov_failure)
		if get_tree().is_network_server():
			lay_card(0)
	else:
		if get_tree().is_network_server():
			next_president_server()


func president_voted():
	print("President voted!")
	print("Chancellor id: %s" % chancellor_id)
	if get_tree().is_network_server():
		rpc_id(chancellor_id, "select_cards", [cards[0], cards[1]])


func shuffle():
	cards += discarded_cards
	cards.shuffle()
	discarded_cards.clear()
	print("Shuffling...")


remotesync func select_cards(_cards: Array):
	print("Select cards")
	assert(1 == get_tree().get_rpc_sender_id())
	var out = "Received cards: "

	for card in _cards:
		out += "%s, " % GameHelper.get_card_type_as_string(card)

	print(out)

	select_cards_popup.display(_cards)


func chancellor_voted():
	print("Chancellor voted")
	lay_card(0)


remotesync func add_fascist_card():  # Adds a point to the facists
	assert(1 == get_tree().get_rpc_sender_id())
	fac_cards += 1
	board_fascist.set_cards(fac_cards)

remotesync func add_liberal_card():  # Adds a point to the liberal
	assert(1 == get_tree().get_rpc_sender_id())
	lib_cards += 1
	board_liberal.set_cards(lib_cards)


func discard_card(index):  # Put one card into the drawn pile
	discarded_cards.append(cards[index])
	cards.remove(index)
	print("Keep: ", cards)
	print("Discarded: ", discarded_cards)
	print("Total count: ", cards.size() + discarded_cards.size() + fac_cards + lib_cards)


master func lay_card(card_index):  # Puts on card on the table
	print("Choosen card: %s" % GameHelper.get_card_type_as_string(cards[card_index]))
	match cards[card_index]:
		Cards.card_type.FASCIST: 
			print("Fascist card")
			rpc("add_fascist_card")
			check_win()
			determine_next_presidential_power()
		Cards.card_type.LIBERAL:
			print("Liberal card")
			rpc("add_liberal_card")
			check_win()
			rpc_id(1, "next_president_server")
		_:
			print("Invalid!")
	cards.remove(card_index)
func determine_next_presidential_power():
	match GameHelper.players.get_child_count():
		5,6:
			match fac_cards:
				3:
					rpc_id(president_id, "show_view_cards", cards.slice(0, 2, 1))
				4,5:
					rpc_id(president_id, "show_to_kill_choice")
				_:
					rpc_id(1,"next_president_server")

		7,8:
			match fac_cards:
				2:
					rpc_id(president_id, "show_investigation_choice")
				3:
					rpc_id(president_id, "show_president_choice")
				4,5:
					rpc_id(president_id, "show_to_kill_choice")
				_:
					rpc_id(1,"next_president_server")
	
		9,10:
			match fac_cards:
				1,2:
					rpc_id(president_id, "show_investigation_choice")
				3:
					rpc_id(president_id, "show_president_choice")
				4,5:
					rpc_id(president_id, "show_to_kill_choice")
				_:
					rpc_id(1,"next_president_server")
		_:
			print("Invalid Player count")
remotesync func next_president_server():
	PlayerHelper.set_president()
	rpc_id(president_id, "show_chancellor_choice")


master func check_win():
	if lib_cards == 5:
		Victory.rpc(
			"victory_init",
			to_json(GameHelper.get_liberals_name()),
			to_json(GameHelper.get_fascists_name()),
			GameConst.winning_side.LIBERALS,
			GameConst.winning_reasons.POLICIES
		)
	if fac_cards == 6:
		Victory.rpc(
			"victory_init",
			to_json(GameHelper.get_liberals_name()),
			to_json(GameHelper.get_fascists_name()),
			GameConst.winning_side.FASCISTS,
			GameConst.winning_reasons.POLICIES
		)		
	for p in GameHelper.get_players():
		if p.role == PlayerConst.player_roles.HITLER:
			if p.is_dead:
				Victory.rpc(
					"victory_init",
					to_json(GameHelper.get_liberals_name()),
					to_json(GameHelper.get_fascists_name()),
					GameConst.winning_side.LIBERALS,
					GameConst.winning_reasons.HITLER_KILED
				)

func refresh_team_members():
	var fascists: Array = GameHelper.get_fascists_id()
	print("Current Fascists: ",fascists)
	for id in GameHelper.get_fascists_id():
		team_members.rpc_id(id, "show_team_members", to_json(fascists),GameHelper.get_hitler_id())
		

remotesync func _on_tempNextPlayer_pressed():
	for player in find_node("Players", true, false).get_children():
		print("-----------------")
		print("ID:", player.player_id)
		print("NAME:", player.player_name)
		print("ROLE:", player.role)
		print("CHANCELLOR:", player.chancellor_info)
		print("PRESIDENT:", player.president_info)
		print("ISDEAD:", player.is_dead)
		print("-----------------")
