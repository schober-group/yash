extends Node


func _ready():
	OS.set_window_title("YASH [" + str(Version.game_version) + "]")

	for argument in OS.get_cmdline_args():
		if argument.match("--name=*"):
			var name = argument.substr(7)
			PlayerConst.player_name = name

		if argument.match("--join=*:*"):
			var server = argument.substr(7).split(":")
			var ip = server[0]
			var port = server[1]
			Network.create_client(ip, int(port))
			var _error = get_tree().change_scene("res://GUI/JoinGame/JoinGame.tscn")

		if argument.match("--host"):
			Network.create_server(2021)
			var _error = get_tree().change_scene("res://GUI/ServerLobby/ServerLobby.tscn")
