extends Node

const SERVER_PORT = 2021
const MAX_PLAYERS = 10

var port


func create_server(server_port: int = SERVER_PORT):
	var peer = _create_peer()
	peer.create_server(server_port, MAX_PLAYERS)
	_save_peer(peer, server_port)


func create_client(server_ip: String, server_port: int = SERVER_PORT):
	print("Create client")
	var peer = _create_peer()
	peer.create_client(server_ip, server_port)
	_save_peer(peer, server_port)


func close():
	get_tree().network_peer = null


func is_open() -> bool:
	return get_tree().network_peer != null


func _create_peer():
	return NetworkedMultiplayerENet.new()


func _save_peer(peer: NetworkedMultiplayerPeer, server_port: int):
	get_tree().network_peer = peer
	port = server_port
