extends Node

signal refresh_gui
signal connected_ok
signal connected_fail

signal start_game

const res_player = "res://Player/Player.tscn"
const res_game = "res://Game/GameUI/GameUI.tscn"

# Player info, associate ID to data
var player_info = {}
var players_done = []  # Players done loading game

# Info we send to other players
var my_info = {name = PlayerConst.player_name}


func _ready():
	var _errors = OK
	_errors += get_tree().connect("network_peer_connected", self, "_player_connected")
	_errors += get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	_errors += get_tree().connect("connected_to_server", self, "_connected_ok")
	_errors += get_tree().connect("connection_failed", self, "_connected_fail")
	_errors += get_tree().connect("server_disconnected", self, "_server_disconnected")
	assert(_errors == OK)


func _player_connected(id):
	print("Player connected")
	# Called on both clients and server when a peer connects. Send my info to it.
	rpc_id(id, "register_player", my_info)


func _player_disconnected(id):
	print("Player disconnected")
	player_info.erase(id)  # Erase player from info.
	emit_signal("refresh_gui")


func _connected_ok():  # Only called on clients, not server. Will go unused; not useful here.
	print("Connected!")
	emit_signal("connected_ok")


func _server_disconnected():
	print("Server disconnected")
	pass  # Server kicked us; show error and abort.


func _connected_fail():  # Could not even connect to server; abort.
	print("Connection failed")
	emit_signal("connected_fail")


remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	player_info[id] = info
	print(info)

	# Call function to update lobby UI here
	emit_signal("refresh_gui")

remotesync func pre_configure_game():
	var selfPeerID = get_tree().get_network_unique_id()

	# Load world
	var game = load(res_game).instance()
	var player_node = game.get_node("Players")

	# Load my player
	var my_player = preload(res_player).instance()
	my_player.name = str(selfPeerID)
	my_player.player_name = my_info.name
	my_player.player_id = selfPeerID
	player_node.add_child(my_player)

	# Load other players
	for p in player_info:
		var player = preload(res_player).instance()
		player.name = str(p)
		player.player_name = player_info[p].name
		player.player_id = str(p)
		player_node.add_child(player)

	$"/root".add_child(game)
	$"/root/ServerLobby".queue_free()

	# Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
	# The server can call get_tree().get_rpc_sender_id() to find out who said they were done.

	print("Is server: " + str(get_tree().is_network_server()))
	if ! get_tree().is_network_server():
		rpc_id(1, "done_preconfiguring")

remote func done_preconfiguring():
	var who = get_tree().get_rpc_sender_id()
	# Here are some checks you can do, for example
	assert(get_tree().is_network_server())
	assert(who in Lobby.player_info)  # Exists
	assert(not who in players_done)  # Was not added yet

	players_done.append(who)

	if players_done.size() == Lobby.player_info.size():
		emit_signal("start_game")
		rpc("post_configure_game")

remote func post_configure_game():
	# Only the server is allowed to tell a client to unpause
	if 1 == get_tree().get_rpc_sender_id():
		get_tree().set_pause(false)
		# Game starts now!


func disconnect_network():
	get_tree().network_peer = null
	player_info = {}
	players_done = []
