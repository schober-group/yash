extends Node

const UNKNOWN_VERSION = "unknown"

var game_version = UNKNOWN_VERSION


func _ready():
	var out = []

	if game_version == UNKNOWN_VERSION:
		if OS.execute("bash Scripts/GetVersion.sh", [], true, out) == OK:  # Linux
			game_version = out[0]
		elif OS.execute("Scripts/GetVersion.cmd", [], true, out) == OK:  # Windows
			game_version = out[0]
		else:
			print("Couldn't determine version :(")
