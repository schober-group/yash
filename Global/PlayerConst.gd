extends Node

enum player_roles { HITLER, FASCIST, LIBERAL }
enum chancellor_state { NOT_CHANCELLOR, TO_ELECT_CHANCELLOR, LAST_CHANCELLOR, CURR_CHANCELLOR }
enum president_state { NOT_PRESIDENT, LAST_PRESIDENT, CURR_PRESIDENT }

var player_name = "A Player" setget set_player_name


func set_player_name(new_value):
	player_name = new_value
	Lobby.my_info.name = player_name


func get_player_role_as_string(role: int) -> String:
	return player_roles.keys()[role]
