extends Node

const game = "/root/Game"
const players = "/root/Game/Players"
const chancellor_choice = "/root/Game/ChancellorChoice"
const chancellor_vote = "/root/Game/ChancellorVote"

enum voting_types { CHANCELLOR, KILL_PLAYER, PRESIDENT, INVESTIGATE }
enum winning_reasons { POLICIES, HITLER_KILED, HITLER_IS_CHANCELLOR }
enum winning_side { LIBERALS, FASCISTS }
