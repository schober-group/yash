extends Node

var liberals
var fascists
var winner_team: int
var reason: int

remotesync func victory_init(_liberals, _fascists, _winner_team: int, _reason: int):
	if get_tree().get_rpc_sender_id() == 1:
		self.liberals = _liberals
		self.fascists = _fascists
		self.winner_team = _winner_team
		self.reason = _reason
		var error = get_tree().change_scene("res://GUI/VictoryScreen/VictoryScreen.tscn")
		print(error)
