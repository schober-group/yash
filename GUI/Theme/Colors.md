# Colors
## General

<span style="color:#ffedae">Button Background ```#ffedae```</span><br>
<span style="color:#ffa700">Logo Color ```#ffa700```</span><br>

## Roles
### Liberal

<span style="color:#173c52">Darker++ ```#173c52```</span><br>
<span style="color:#307fac">Darker ```#307fac```</span><br>
<span style="color:#3d96c9">Default ```#3d96c9```</span><br>
<span style="color:#c4e9ff">Lighter ```#c4e9ff```</span><br>
<span style="color:#f4fbff">Lighter++ ```#f4fbff```</span><br>

### Fascist

<span style="color:#340d0c">Darker++ ```#340d0c```</span><br>
<span style="color:#bf302c">Darker ```#bf302c```</span><br>
<span style="color:#d6514e">Default ```#d6514e```</span><br>
<span style="color:#ffd2d2">Lighter ```#ffd2d2```</span><br>
<span style="color:#ffeeee">Lighter++ ```#ffeeee```</span><br>
