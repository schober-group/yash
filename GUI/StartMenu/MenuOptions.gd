extends VBoxContainer


func _on_JoinGame_pressed():
	var _error = get_tree().change_scene("res://GUI/JoinGame/JoinGame.tscn")


func _on_HostGame_pressed():
	var _error = get_tree().change_scene("res://GUI/HostGame/HostGame.tscn")


func _on_Exit_pressed():
	get_tree().quit()
