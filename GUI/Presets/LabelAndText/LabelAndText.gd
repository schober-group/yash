extends HBoxContainer

export var label = "Label"
export var default_value = ""

onready var label_node = $Label
onready var edit_line_node = $LineEdit


func _ready():
	label_node.text = label
	edit_line_node.text = default_value


func get_value():
	return edit_line_node.text


var screen_height = ProjectSettings.get_setting("display/window/size/height")
var actual_resolution = OS.get_window_size()
var set_position = true
var global_position


func reposition():
	var target_y
	if edit_line_node.has_focus() and OS.get_virtual_keyboard_height() != 0:
		var ratio = screen_height / actual_resolution.y
		target_y = min(
			global_position.y,
			(screen_height - get_size().y - (OS.get_virtual_keyboard_height() * ratio)) / 2
		)
	else:
		target_y = global_position.y
	set_global_position(Vector2(global_position.x, target_y))


func _process(_delta):
	if set_position:
		global_position = get_global_position()
		set_position = false
	reposition()
