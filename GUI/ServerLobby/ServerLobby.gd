extends MarginContainer

export var my_color = "#7289da"

onready var player_list = $NinePatchRect/CenterContainer/VBoxContainer/ScrollContainer/PlayerList
onready var not_enough_players_popup = $NinePatchRect/ErrorPopup
onready var loading_screen = $NinePatchRect/LoadingPopup


func _ready():
	var _errors = OK
	_errors += Lobby.connect("refresh_gui", self, "set_players")
	_errors += get_tree().connect("server_disconnected", self, "_back")
	assert(_errors == OK)
	set_players()


func set_players():
	_set_myself()
	for player in Lobby.player_info:
		player_list.append_bbcode("\n" + Lobby.player_info[player].name)


func _set_myself():
	player_list.bbcode_text = "[color=" + my_color + "]" + Lobby.my_info.name + "[/color]"


func _on_ButtonStartGame_pressed():
	#if(Lobby.player_info.size() >= 5):
	if true:
		print("Starting game...")
		loading_screen.popup()
		Lobby.rpc("pre_configure_game")
	else:
		not_enough_players_popup.popup()


func _on_BackButton_pressed():
	_back()


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		_back()


func _back():
	var _error = get_tree().change_scene("res://GUI/StartMenu/StartMenu.tscn")
	Lobby.disconnect_network()

	if _error != OK:
		print("Could not load StartMenu")
