extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var winners_text = $CenterContainer/HBoxContainer/InfoBox/WinningSide
onready var reason_text = $CenterContainer/HBoxContainer/InfoBox/WinningReason
onready var libs = $NinePatchRect/Liberals
onready var fascs = $NinePatchRect/Fascists
var winning_reason_text = ["All policies were enacted", "Hitler was killed", "Hitler is Chancellor"]
var winning_party = ["The Liberals have won", "The Fascists have won"]


# Called when the node enters the scene tree for the first time.
func _ready():
	var lib_array = parse_json(Victory.liberals)
	var fas_array = parse_json(Victory.fascists)

	init(lib_array, fas_array, Victory.winner_team, Victory.reason)
	GameHelper.game.queue_free()


func init(liberals: Array, fascists: Array, winner: int, reason: int):
	reason_text.text = winning_reason_text[reason]
	winners_text.text = winning_party[winner]

	for p in liberals:
		var label = Label.new()
		label.name = p
		label.text = p
		libs.add_child(label)
	for p in fascists:
		var label = Label.new()
		label.name = p
		label.text = p
		fascs.add_child(label)


func _on_Button_pressed():
	var _error = get_tree().change_scene("res://GUI/StartMenu/StartMenu.tscn")
	print(_error)
	$".".queue_free()
	Lobby.disconnect_network()
