extends PopupPanel

export var error_msg = "Couldn't connect"

onready var label = $MarginContainer/VBoxContainer/Label


func _ready():
	var _error = Lobby.connect("connected_fail", self, "abort")
	label.text = error_msg


func abort():
	$".".popup()


func _on_Button_pressed():
	$".".hide()
