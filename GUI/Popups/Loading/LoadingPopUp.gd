extends PopupPanel


func _ready():
	var _error = OK
	_error += Lobby.connect("connected_ok", self, "load_game")
	_error += Lobby.connect("connected_fail", self, "abort")
	assert(_error == OK)


func load_game():
	_hide_popup()
	var _error = get_tree().change_scene("res://GUI/ServerLobby/ServerLobby.tscn")


func abort():
	_hide_popup()


func _hide_popup():
	$".".hide()
