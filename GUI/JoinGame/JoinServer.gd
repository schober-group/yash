extends Button

onready var port = $"../../CenterContainer/VBoxInputs/Port"
onready var ip = $"../../CenterContainer/VBoxInputs/IP"
onready var popup = $"../LoadingPopup"


func _ready():
	if Network.is_open():
		call_deferred("_show_popup")


func connect_to_server():
	Network.create_client(ip.get_value(), int(port.get_value()))
	_show_popup()


func _on_JoinServer_pressed():
	connect_to_server()


func _show_popup():
	popup.popup()
