extends MarginContainer


func _ready():
	get_tree().set_auto_accept_quit(false)
	pass


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		_back()


func _on_BackButton_pressed():
	_back()


func _back():
	var _error = get_tree().change_scene("res://GUI/StartMenu/StartMenu.tscn")

	if _error != OK:
		print("Could not load StartMenu")
