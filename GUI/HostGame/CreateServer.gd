extends Button

onready var port = $"../../CenterContainer/VBoxInputs/Port"


func _on_CreateServer_pressed():
	Network.create_server(port.get_value() as int)
	var _error = get_tree().change_scene("res://GUI/ServerLobby/ServerLobby.tscn")
	if _error != OK:
		print("Could not load ServerLobby")
