#!/bin/bash

PROJECT_ROOT="$(pwd)"
Scripts/SetVersion.sh

if [ $BUILD_FOLDER -n ]; then
    BUILD_FOLDER=Build
fi

for arg in "$@"; do
    case "${arg}" in
    --linux | -l)
        LINUX=YES
        ;;
    --windows | -w)
        WINDOWS=YES
        ;;
    --macos | -m)
        MACOS=YES
        ;;
    --android-debug | -ad)
        ANDROID_DEBUG=YES
        ;;
    esac
done

function godotBuild() {
    local name="$1"
    local platform="$2"
    local outfile="$3"
    local debug="$4"
    local FOLDER="$PROJECT_ROOT/$BUILD_FOLDER/$platform"

    mkdir -v -p "$FOLDER"

    if [ "$debug" == true ]; then
        godot -v --export-debug "$name" "$FOLDER/$outfile"
    else
        godot -v --export "$name" "$FOLDER/$outfile"
    fi
}

if [ "$LINUX" == "YES" ]; then
    godotBuild "Linux/X11" "linux" "yash.x86_64"
fi

if [ "$WINDOWS" == "YES" ]; then
    godotBuild "Windows Desktop" "windows" "yash.exe"
fi

if [ "$MACOS" == "YES" ]; then
    godotBuild "Mac OSX" "macos" "yash.zip"
fi

if [ "$ANDROID_DEBUG" == "YES" ]; then
    godotBuild "Android Debug" "android-debug" "yash-debug.apk" "true"
fi
