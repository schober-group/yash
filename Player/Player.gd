extends Node

class_name Player

var player_id: int = -1
var player_name = "DEFAULT_FRANZI"
var role = PlayerConst.player_roles.HITLER
var chancellor_info = PlayerConst.chancellor_state.NOT_CHANCELLOR setget set_chancellor_info
var president_info = PlayerConst.president_state.NOT_PRESIDENT setget set_president_info
var is_dead = false


func kill_player():
	is_dead = true
	GameHelper.game.rpc_id(1, "check_win")


func set_chancellor_info(new_info: int):
	match new_info:
		PlayerConst.chancellor_state.CURR_CHANCELLOR:
			GameHelper.game.chancellor_id = player_id
		PlayerConst.chancellor_state.LAST_CHANCELLOR:
			GameHelper.game.last_chancellor_id = player_id

	chancellor_info = new_info


func set_president_info(new_info: int):
	match new_info:
		PlayerConst.president_state.CURR_PRESIDENT:
			GameHelper.game.president_id = player_id
		PlayerConst.president_state.LAST_PRESIDENT:
			GameHelper.game.last_president_id = player_id

	president_info = new_info


remotesync func set_role(_role: int):
	assert(1 == get_tree().get_rpc_sender_id())
	role = _role
	GameHelper.game.temp_role.text = PlayerConst.get_player_role_as_string(role)
